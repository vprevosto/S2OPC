# Continuous Integration configuration with gitlab.com
# See https://gitlab.com/help/ci/yaml/README.md


#########################################################
# Default CI pipeline configuration:
# - preset variables: 'WITH_NANO_EXTENDED = 1' set in gitlab project
# - jobs run in stages:
#   - gen: generation job
#   - build: build-linux64-ASan
#   - tests:
#     - test-check
#     - test-unit
#     - test-uactt
#
# CI pipeline manual run with 'ALL_TESTS = 1':
# - preset variables: 'WITH_NANO_EXTENDED = 1' set in gitlab project
# - jobs run in stages:
#   - build: build-linux64-pys2opc # 'WITH_ASAN = 0' and 'WITH_PYS2OPC = 1'
#   - tests: # TEST_SERVER_XML_ADDRESS_SPACE and TEST_SERVER_XML_CONFIG set to use dynamic config
#     - test-unit-all-tests
#     - test-uactt-all-tests
#
# CI pipeline manual run with 'ALL_BUILDS = 1':
# - preset variables: WITH_NANO_EXTENDED = 1 set in gitlab project
# - jobs run in stages:
#   - gen: generation job
#   - build: # 'WITH_STATIC_SECURITY_DATA: 1', 'WITH_CONST_ADDSPACE: 1' and 'PUBSUB_STATIC_CONFIG: 1'
#     - build-linux64-static-conf
#   - tests:
#     - test-unit
#   - build-others:
#     - build-win32
#     - build-win64
#     - windows-appveyor
#
# CI pipeline manual run with 'WINDOWS_TEST = 1':
# - jobs run in stages:
#   - build-others:
#     - windows-appveyor
#
# CI pipeline manual run with 'CROSS_COMPILE = 1':
# - jobs run in stages:
#   - build-others:
#     - build-win32
#     - build-win64
#########################################################

# Variables for ALL_BUILDS and ALL_TESTS manual pipelines

.variables: &all-tests_build_variables
  WITH_PYS2OPC: 1

.variables: &all-tests_test_variables
  TEST_SERVER_XML_ADDRESS_SPACE: s2opc.xml
  TEST_SERVER_XML_CONFIG: S2OPC_Server_UACTT_Config.xml
  TEST_USERS_XML_CONFIG: S2OPC_UACTT_Users.xml

.variables: &all-builds_build_variables
  WITH_STATIC_SECURITY_DATA: 1
  WITH_CONST_ADDSPACE: 1
  PUBSUB_STATIC_CONFIG: 1

stages:
  - gen
  - build
  - test
  - build-others
  - analyses
  - doc

.docker-job: &docker_job
  tags:
    - docker
    - linux

###
# Generation jobs
###

generation:
  <<: *docker_job
  stage: gen
  image: com.systerel.fr:5000/c765/gen-ext@sha256:29c8f93dfce606b86133605ee367c14e3b4b978eebe4181e42e9b826db06b06c # digest of gen-ext:1.3
  script:
    - ./clean.sh all
    - ./.pre-build-ext.sh
    - ./.check_generated_code.sh
  artifacts:
    name: 'bgenc-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'pre-build.log'
    when: always
    expire_in: '1 month'
    when: always
  rules:
    - if: "$NO_GEN == null && \
           $WINDOWS_TEST == null && \
           $CROSS_COMPILE == null && \
           $S2OPC_PUBSUB_ONLY == null &&\
           $ALL_BUILDS == null && \
           $ALL_TESTS == null && \
           $WITH_DOC == null"
      when: on_success

###
# Compilation jobs
###

.build-linux: &build_linux
  <<: *docker_job
  stage: build
  image: registry.gitlab.com/systerel/s2opc/build@sha256:fe5542520a5ea7b975f48da16d65c6dd641f77826784c862d730a2b75c4b8ea8 # digest of build:1.26
  script:
    - &build_command_line './build.sh && cd $BUILD_DIR && cmake --build . --target install || exit 1'
  variables: &build_linux_variables
    CMAKE_INSTALL_PREFIX: '../install_linux64'
    DOCKER_IMAGE: 'sha256:0bbab53a4c13efb85aaca8fbfddc60e60acb7a776b3491ae75715c21d88b3942'
    BUILD_DIR: build
  artifacts:
    name: 'bin-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '*.log'
      - 'install_linux64/'
      - 'build/'  # Required when using coverage analysis
    expire_in: '1 month'
    when: always
# Common rules overwritten by each job using this template
#  rules: # note: should be repeated in each child job of *build_linux
#    - if: "$WINDOWS_TEST == null && $CROSS_COMPILE == null && $WITH_COVERITY == null && $WITH_DOC == null"
#      when: on_success

# Used by "normal" pipeline
build-linux64-ASan:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    WITH_ASAN: 1
    WITH_UBSAN: 1
  rules: # 1st line repeat .build-linux + added rules to avoid ASAN build
    - if: "$WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && $WITH_DOC == null && \
           $NO_ASAN == null && $ALL_TESTS == null && $ALL_BUILDS == null" # avoid ASAN build in most of nightly builds
      when: on_success

# Used by all-tests pipeline (add tests which needs NO_ASAN)
build-linux64-pys2opc:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    <<: *all-tests_build_variables # add pys2opc
  rules: # do not repeat .build-linux: due to disjunction + need only those 2 conditions
    - if: "$ALL_TESTS != null || $NO_ASAN != null && $WITH_NANO_EXTENDED == '1'" # if ASAN is deactivated build PyS2OPC by default (exclude nano scope: see TODO below)
      when: on_success

# Used when ALL_BUILDS set (tests use static configuration)
build-linux64-static-conf:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    <<: *all-builds_build_variables # add static conf
  rules: # single case rule of ALL_BUILDS
    - if: "$ALL_BUILDS != null"
      when: on_success

build-linux64-NoASan-NoPyS2OPC:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
  rules: # In case of coverage analysis or when nano scope is used (TODO: PyS2OPC shall be built but tests will fail: use another server with subscription in tests)
    - if: "$WITH_COVERAGE != null || $NO_ASAN != null && $WITH_NANO_EXTENDED != '1'"
      when: on_success

build-win64: &build_win64
  <<: *docker_job
  stage: build-others
  image: registry.gitlab.com/systerel/s2opc/mingw_build@sha256:b9d5619a72109f24f8fe4c7cce03216153542cd20734a9eaae01104ca5166b8a # digest of mingw_build:1.12
  script:
    # Manually disable PyS2OPC for windows cross builds. When set in "variables:", the value is overwritten.
    - 'export WITH_PYS2OPC=OFF'
    - *build_command_line
  variables: &build_win64_variables
    CMAKE_INSTALL_PREFIX: '../install_win64'
    CMAKE_TOOLCHAIN_FILE: 'toolchain-mingw32-w64_x86_64.cmake'
    DOCKER_IMAGE: 'sha256:3d552edb5eab18138ef4ad6652b6da8fef00dc36dc6b9fd9fe890ffff513c3e5'
    BUILD_SHARED_LIBS: 'true'
    BUILD_DIR: build_toolchain
    S2OPC_CLIENTSERVER_ONLY: 1 # PubSub is not compatible with windows platform
  artifacts:
    name: 'bin-w64-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '$BUILD_DIR/bin'
      - 'install_win64/'
    expire_in: '1 month'
  rules:
   - if: "$CROSS_COMPILE != null || $ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed

build-win32:
  <<: *build_win64
  variables:
    <<: *build_win64_variables
    CMAKE_INSTALL_PREFIX: '../install_win32'
    CMAKE_TOOLCHAIN_FILE: 'toolchain-mingw32-w64.cmake'
  artifacts:
    name: 'bin-w32-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '$BUILD_DIR/bin/'
      - 'install_win32/'
    expire_in: '1 month'
  rules:
   - if: "$CROSS_COMPILE != null || $ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed

build-rpi:
  <<: *docker_job
  stage: build-others
  image: registry.gitlab.com/systerel/s2opc/rpi-build@sha256:977b5d7083659af98a6279ee12d7d3ca1c94195b8b954c72c605a22386446a7c # digest of rpi-build:1.4
  script:
    - *build_command_line
  variables:
    CMAKE_INSTALL_PREFIX: '../install_rpi'
    CMAKE_TOOLCHAIN_FILE: '/toolchain-rpi.cmake'
    DOCKER_IMAGE: 'sha256:1d0eb4c4f99214faf186b4af677f138ef6e01945053e1b7533c99edfdf469f0f'
    BUILD_SHARED_LIBS: 'true'
    BUILD_DIR: build_toolchain
  artifacts:
    name: 'bin-rpi-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '$BUILD_DIR/bin'
      - 'install_rpi/'
    expire_in: '1 month'
  rules:
   - if: "$CROSS_COMPILE != null || $ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed

# Used by coverity, also starts the analysis
build-linux64-coverity:
  <<: *docker_job
  stage: build
  image: registry.gitlab.com/systerel/s2opc/coverity@sha256:ba1676af8d9f52cf6c2634af775a63eae9324c281590d6bc29985e9afedb36a4 # digest of coverity:1.5
  script: |-
    mkdir build
    cd build
    cmake -DWITH_COVERITY=1 -DCMAKE_BUILD_TYPE=Debug -DWITH_NANO_EXTENDED=1 ..
    /opt/cov-analysis-linux64-2019.03/bin/cov-build --dir cov-int make
    tar czf cov-int.tar.gz cov-int
    curl -k --form token=${COVERITY_TOKEN} --form email=${COVERITY_EMAIL} --form version=${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA} --form description=${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA} --form file=@cov-int.tar.gz https://scan.coverity.com/builds?project=S2OPC
  rules:
    - if: "$WITH_COVERITY != null"

###
# Test jobs
###

.test-linux: &test_linux
  <<: *docker_job
  stage: test
# Common rules overwritten by each job using this template
#  rules: # note to be repeated by each child job of *test_linux
#    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
#           $WITH_COVERITY == null && $WITH_COVERAGE == null"
#      when: on_success

# Test check belongs to the test part, however it does not depend on linux build.
test-check:
  <<: *test_linux
  image: registry.gitlab.com/systerel/s2opc/check@sha256:6805805e14420a9b635c2305526a4535f9edd191ff22801fc84cca94410b1a03 # digest of check:1.14
  script:
    - ./.check-code.sh
  variables:
      DOCKER_IMAGE: 'sha256:20d4e72a3c1681a55aeb7b083a28edf4b58d12d113fc9271806d8e70900000ff'
  artifacts:
    name: 'test-check-results-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'pre-build-check.log'
      - 'clang_tidy.log'
      - 'build-analyzer/analyzer-report'
    when: on_failure
    expire_in: '1 month'
  rules: # repeat .test-linux variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && $WITH_DOC == null && \
           $ALL_TESTS == null && $ALL_BUILDS == null"
      when: on_success

.test-unit: &test_unit
  <<: *test_linux
  image: registry.gitlab.com/systerel/s2opc/test@sha256:349996835b968631da208389301bbaf76d05cd8237946c739613a6551220c6aa  # digest of test:2.9
  script:
    - './test-all.sh'
  artifacts:
    name: 'test-unit-results-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'build/'
    when: always
    expire_in: '1 month'
# Common rules defined by test-unit job below

# Test unit for ALL_TESTS variable (add dynamic configuration)
test-unit-all-tests:
  <<: *test_unit
  variables:
    <<: *all-tests_test_variables
  script:
    - './test-all.sh'
  rules:
    - if: "$ALL_TESTS != null"
      when: on_success

# Test unit for NOT ALL_TESTS variable
test-unit:
  <<: *test_unit
  rules: # repeat .test-linux variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_DOC == null && \
           $ALL_TESTS == null" # + excludes ALL_TESTS
      when: on_success

.test-uactt: &test_uactt
  <<: *test_linux
  image: com.systerel.fr:5000/c838/uactt-win@sha256:5be484563b6d9ba9bc96ace1e0f4a7c2f44acbf44197db7ef020a24beb140e03 # digest of uactt-win:1.8
  script:
    - 'cd tests/ClientServer/acceptance_tools/ && ./launch_acceptance_tests.sh'
  artifacts:
    name: 'uactt-logs-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'build/'
      - 'tests/ClientServer/acceptance_tools/*.log'
    when: always
    expire_in: '1 month'

# Test unit for ALL_TESTS variable (add dynamic configuration)
test-uactt-all-tests:
  <<: *test_uactt
  variables:
    <<: *all-tests_test_variables
  rules:
    - if: "$ALL_TESTS != null && \
           $S2OPC_PUBSUB_ONLY == null && \
           $ALL_BUILDS == null" # + no UACTT tests on PubSub + excludes ALL_BUILDS

# Test unit for NOT ALL_TESTS variable
test-uactt:
  <<: *test_uactt
  rules: # repeat .test-uactt variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && $WITH_DOC == null && \
           $S2OPC_PUBSUB_ONLY == null && $ALL_BUILDS == null && $ALL_TESTS == null"
           # + no UACTT tests on PubSub + excludes ALL_BUILDS + excludes ALL_TESTS
      when: on_success

##
# Windows native build and tests (done in "build-others" stage)
##

windows-appveyor:
  stage: build-others
  image: docker.io/library/debian@sha256:de3eac83cd481c04c5d6c7344cd7327625a1d8b2540e82a8231b5675cef0ae5f  # debian:9
  script:
    - 'apt-get update && apt-get -y install curl && curl -H "Authorization: Bearer $APPVEYOR_TOKEN" -H "Content-Type: application/json" --request POST -d ''{"accountName" : "Systerel", "projectSlug": "s2opc", "branch": "''$CI_COMMIT_REF_NAME''"}'' https://ci.appveyor.com/api/builds'
  tags:
    - linux
  rules:
    - if: "$ALL_BUILDS != null || $WINDOWS_TEST != null || \
           $CI_COMMIT_BRANCH == 'master' && \
           $ALL_TESTS == null && $WITH_COVERITY == null && $WITH_COVERAGE == null && \
           $NO_ASAN == null && $NO_GEN == null && \
           $S2OPC_PUBSUB_ONLY == null && $S2OPC_CLIENTSERVER_ONLY == null" # Avoid to run job on master for all nightly build except ALL_BUILDS
      when: always # run even if previous job in pipeline failed

###
# Analysis jobs
###

coverage-analysis:
  <<: *docker_job
  stage: analyses
  image: registry.gitlab.com/systerel/s2opc/test@sha256:349996835b968631da208389301bbaf76d05cd8237946c739613a6551220c6aa  # digest of test:2.9
  script:
    - ./.gen_coverage.sh
  artifacts:
    name: 'coverage-report-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'report'
    expire_in: '1 month'
  rules:
    - if: "$WITH_COVERAGE != null"

###
# Documentation generation and publication
###

# The job name must be "pages", and the html must be in "public/" and put in an artifact.
pages:
  <<: *build_linux
  stage: doc
  dependencies: []
  rules:
    - if: "$WITH_DOC != null"
    - if: "$CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == 'master'"
  variables:
    <<: *build_linux_variables
  script:
    - mkdir $BUILD_DIR
    - cd $BUILD_DIR
    # PyS2OPC relies on subscriptions
    - cmake -DWITH_NANO_EXTENDED=yes -DWITH_PYS2OPC=yes ..
    - cmake --build . --target doc
    - mv apidoc/html ../public/
  artifacts:
    paths:
    - public
    expire_in: 30 days

check_doc:
  <<: *build_linux
  stage: doc
  script:
    - cd $BUILD_DIR
    - cmake --build . --target doc &> doc.log
    - test $(grep -c warning doc.log) -eq 0
  variables:
    <<: *build_linux_variables
  artifacts:
    paths:
      - '$BUILD_DIR/doc.log'
    expire_in: '1 month'
    when: on_failure
  rules:
   - if: "$ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed
